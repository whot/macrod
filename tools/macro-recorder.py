#!/usr/bin/env python3
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.

import argparse
import logging
import sys
import libevdev


logging.basicConfig(format='%(levelname)s: %(name)s: %(message)s',
                    level=logging.INFO)
logger = logging.getLogger('recorder')


class Recorder(object):
    RELEASE = 0
    PRESS = 1
    TYPE = 2

    def __init__(self):
        self._reset()

    def _reset(self):
        self.events = []

    def record(self, path):
        self._reset()

        source_fd = open(path, 'rb')
        dev = libevdev.Device(source_fd)
        print('Ready to record, press Ctrl+C to finish.')

        for e in dev.events():
            if not e.matches(libevdev.EV_KEY):
                continue

            # skip key repeats
            if e.value not in [0, 1]:
                continue

            # skip key releases after startup
            if e.value == 0 and not self.events:
                continue

            try:
                last = self.events[-1]
                if (last.code == e.code and
                        last.value == Recorder.PRESS and
                        e.value == Recorder.RELEASE):
                    last.value = Recorder.TYPE
                else:
                    self.events.append(e)
            except IndexError:
                # The first event is always appended
                self.events.append(e)

            logger.debug(f'Key {e.code.name} {"press  " if e.value else "release"}')

    def strip_ctrl_c(self):
        # Search backwards to the last pressed-only control key and strip
        # that and anything after. This will do nothing if the user has a
        # control key remapping to some other key. Oh well.
        for idx in range(len(self.events) - 1, -1, -1):
            e = self.events[idx]
            if e.code in [libevdev.EV_KEY.KEY_LEFTCTRL, libevdev.EV_KEY.KEY_RIGHTCTRL]:
                if e.value != Recorder.PRESS:
                    # Found a control key, but it was released, so this
                    # isn't a ctrl+c
                    return
                else:
                    logger.debug(f'Stripping trailing CTRL+C')
                    self.events = self.events[:idx]

    def as_string(self):
        self.strip_ctrl_c()

        strings = []
        for e in self.events:
            prefixes = {
                Recorder.PRESS: '+',
                Recorder.RELEASE: '-',
                Recorder.TYPE: '',
            }
            prefix = prefixes[e.value]
            name = e.code.name.replace('KEY_', '')
            strings.append(f'{prefix}{name}')
        return ";".join(strings)


def main(args=sys.argv):
    if sys.version_info < (3, 6):
        sys.exit('Python 3.6 or later required')

    desc = '''
Macro recorder for macrod. This tool parses key events from the
given device node and converts it into a string that can be
copied into a macrod configuration file.

This tool needs read access to the device node, run it as root.

This tool does not record timing information between keys, type as fast or
as slowly as you like.
'''
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-v', '--verbose',
                        help='Show some debugging informations',
                        action='store_true',
                        default=False)
    parser.add_argument('device',
                        metavar='/dev/input/eventX',
                        help='The device to monitor',
                        action='store')
    ns = parser.parse_args(args[1:])
    if ns.verbose:
        logger.setLevel(logging.DEBUG)

    recorder = Recorder()
    try:
        recorder.record(ns.device)
    except KeyboardInterrupt:
        pass

    macro = recorder.as_string()
    print('\rRecorded macro sequence:')
    print(macro)
    print('Use the above line in your configuration file as value for e.g. "OnKeyPress="')


if __name__ == '__main__':
    main()
