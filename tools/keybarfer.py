#!/usr/bin/env python3
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
# This is a Proof Of Context
#
# KeyBarfer requires access to /dev/uinput and the source device
# (/dev/input/eventX). The easiest solution for this
#
# Example configuration file:
#
# [Profile]
# Name=none
# ClientMatch=keybarfer
#
# [type-hello]
# KeyTrigger=KEY_A
# KeySequencePress=H;E;L;L;O
#
# [type-world]
# KeyTrigger=KEY_B
# KeySequencePress=W;O;R;L;D

import argparse
import logging
import pwd
import os
import sys

import libevdev

try:
    from gi.repository import GObject, GLib, Gio
except Exception as e:
    print(f'************ Importing gi.repository failed ************')
    print(f'* This is an issue with the gi module, not with macrod *')
    print(f'********************************************************')
    print(f'The full exception is below:')
    print(f'')
    raise e

logging.basicConfig(format='%(levelname)s: %(name)s: %(message)s',
                    level=logging.INFO)
logger = logging.getLogger('keybarfer')


class _DBusObj(GObject.GObject):
    _dbus = None

    def __init__(self, interface, object_path):
        super().__init__()

        if _DBusObj._dbus is None:
            _DBusObj._dbus = Gio.bus_get_sync(Gio.BusType.SESSION, None)

        macrod1 = 'org.freedesktop.macrod1'
        if object_path is None:
            object_path = '/' + macrod1.replace('.', '/')

        self.object_path = object_path
        self._interface = f'{macrod1}.{interface}'

        self._proxy = Gio.DBusProxy.new_sync(_DBusObj._dbus,
                                             Gio.DBusProxyFlags.NONE,
                                             None,
                                             macrod1,
                                             object_path,
                                             self._interface,
                                             None)
        if self._proxy.get_name_owner() is None:
            logger.error(f'Name {macrod1} is not owned, macrod is not running.')
            raise Exception

        self._proxy.connect("g-properties-changed", self._on_properties_changed)
        self._proxy.connect("g-signal", self._on_signal_received)

    def _on_properties_changed(self, proxy, changed_props, invalidated_props):
        # Implement this in derived classes to respond to property changes.
        pass

    def _on_signal_received(self, proxy, sender_name, signal_name, parameters):
        # Implement this in derived classes to respond to signals.
        pass

    def _get_dbus_property(self, property):
        # Retrieves a cached property from the bus, or None.
        p = self._proxy.get_cached_property(property)
        if p is not None:
            return p.unpack()
        return p

    def _dbus_call(self, method, argtype=None, *value):
        # Calls a method synchronously on the bus, using the given method name,
        # type signature and values.
        #
        # It the result is valid, it is returned. Invalid results raise the
        # appropriate RatbagError* or RatbagdDBus* exception, or GLib.Error if
        # it is an unexpected exception that probably shouldn't be passed up to
        # the UI.
        need_fds = False
        if argtype is not None:
            val = GLib.Variant("({})".format(argtype), value)
            if 'h' in argtype:
                need_fds = True
        else:
            val = None

        # This only handles one fd argument. Enough for us for now.
        fd_list = None
        if need_fds:
            fd_list = Gio.UnixFDList.new()
            fd_list.append(value[0])
            res = self._proxy.call_with_unix_fd_list_sync(method, val,
                                                          Gio.DBusCallFlags.NO_AUTO_START,
                                                          2000, fd_list, None)
        else:
            res = self._proxy.call_sync(method, val,
                                        Gio.DBusCallFlags.NO_AUTO_START,
                                        2000, None)
        return res

    def _dbus_call_return_one_fd(self, method, argtype=None, *value):
        # Calls a method synchronously on the bus, using the given method name,
        # type signature and values.
        #
        # It the result is valid, it is returned. Invalid results raise the
        # appropriate RatbagError* or RatbagdDBus* exception, or GLib.Error if
        # it is an unexpected exception that probably shouldn't be passed up to
        # the UI.
        if argtype is not None:
            val = GLib.Variant("({})".format(argtype), value)
        else:
            val = None
        res = self._proxy.call_with_unix_fd_list_sync(method, val,
                                                      Gio.DBusCallFlags.NO_AUTO_START,
                                                      2000, None, None)
        # Result is a tuple of (GLib.Variant('(h)', (0,)), fd_list),
        # we return the first file descriptor in that list
        return res[1].get(0)

    def __eq__(self, other):
        return other and self._object_path == other._object_path


class Macrod(_DBusObj):
    def __init__(self, source_fd, uinput_fd):
        super().__init__("Macrod", None)
        self.returncode = 0

        kbd = self._dbus_call('GetKeyboard', 's', 'keybarfer').unpack()[0]
        self._keyboard = MacrodDevice(kbd, source_fd, uinput_fd)
        self.mainloop = GLib.MainLoop()
        self._proxy.connect("notify::g-name-owner", self._on_name_owner_changed)

    def _on_name_owner_changed(self, *kwargs):
        logger.error('Name owner lost')
        self.cleanup()
        self.returncode = 1
        self.mainloop.quit()

    def run(self):
        self.mainloop.run()

    def cleanup(self):
        self._keyboard.destroy()


class MacrodDevice(_DBusObj):
    def __init__(self, objpath, source_fd, uinput_fd):
        super().__init__("Device", objpath)

        self._evdev = libevdev.Device(source_fd)
        self._fd_glib_source = GLib.io_add_watch(source_fd, GLib.IO_IN, self._on_evdev_data)
        logger.debug(f'Source device: {self._evdev.name}')

        evbits = self._get_dbus_property('EvdevBits')
        d = libevdev.Device()
        d.name = 'KeyBarfer keyboard'
        for bit in evbits:
            b = libevdev.evbit(bit[0], bit[1])
            d.enable(b)

        self._uinput = d.create_uinput_device(uinput_fd)
        logger.debug(f'Our fake device: {self._uinput.devnode}')

        self._dbus_call('SetEvdevFd', 'h', uinput_fd.fileno())

    def destroy(self):
        GLib.source_remove(self._fd_glib_source)
        self._dbus_call('Destroy')

    def _on_evdev_data(self, source, cb_condition):
        logger.debug('Evdev device has data')
        for e in self._evdev.events():
            if not e.matches(libevdev.EV_KEY):
                continue

            if e.value not in [0, 1]:
                continue

            if e.value == 1:
                func = 'KeyPress'
            else:
                func = 'KeyRelease'

            self._dbus_call(func, 'u', e.code.value)
        return True


def drop_privileges():
    logger.debug('dropping privileges\n')

    os.setgroups([])
    gid = int(os.getenv('SUDO_GID'))
    uid = int(os.getenv('SUDO_UID'))
    pwname = os.getenv('SUDO_USER')
    os.setresgid(gid, gid, gid)
    os.initgroups(pwname, gid)
    os.setresuid(uid, uid, uid)

    pw = pwd.getpwuid(uid)

    # we completely clear the environment and start a new and controlled one
    os.environ.clear()
    os.environ['XDG_RUNTIME_DIR'] = f'/run/user/{uid}'
    os.environ['HOME'] = pw.pw_dir


def main(args=sys.argv):
    if sys.version_info < (3, 6):
        sys.exit('Python 3.6 or later required')

    desc = 'uinput bridge for macrod'
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-v', '--verbose',
                        help='Show some debugging informations',
                        action='store_true',
                        default=False)
    parser.add_argument('device',
                        metavar='/dev/input/eventX',
                        help='The device to monitor',
                        action='store')
    ns = parser.parse_args(args[1:])
    if ns.verbose:
        logger.setLevel(logging.DEBUG)

    source_fd = open(ns.device, 'rb')
    os.set_blocking(source_fd.fileno(), False)

    uinput_fd = open('/dev/uinput', 'wb')

    if os.geteuid() == 0:
        drop_privileges()

    macrod = Macrod(source_fd, uinput_fd)
    try:
        macrod.run()
    except KeyboardInterrupt:
        pass
    macrod.cleanup()

    sys.exit(macrod.returncode)


if __name__ == '__main__':
    main()
