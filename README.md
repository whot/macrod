macrod
======

macrod is a daemon for generic keyboard macros. macrod takes over
configuration and event generation for macro keys but generally leaves most
of the integration bits up to the caller.

How it works
------------

macrod is a DBus daemon that has a per-client API. A DBus client such as a
Wayland compositor can request an `evdevfd` from macrod. This `evdevfd`
sends evdev events and can thus be passed to anything that handles evdev
devices.

The caller can select a **profile** for that fd. This profile defines which
keys are mapped to macros in macrod.

The caller then passes events to macrod. Filtering the event is left to the
caller. A Wayland compositor would usually pass the key events to macrod and
otherwise discard that event, on the assumption that it would trigger the
event anyway.

A privileged process may create a uinput device and connect that to the
macrod `evdevfd`, making the macros directly available to any process.
Note that this will only work for keys not used elsewhere, any keys already
assigned keycodes or macros will cause duplicate events.

Example
-------

Let's take the example of a Wayland compositor using libinput, e.g. mutter.
Input events from real devices come in like this:

```
+-------------------+            +----------+                 +--------+
| /dev/input/event0 | -[evdev]-> | libinput | -[key events]-> | mutter |
+-------------------+            +----------+                 +--------+
```

If we want the key events to trigger macros, mutter creates a libinput user
device (not yet implemented) based on `socket(2)` and provides that socket to
macrod.

```
+-------------------------+      +-----------+
| libinput                |      | mutter    |
|   +---------------------+      |  +--------+
|   |libinput_user_dev(fd)|<--------|socket()|
+-------------------------+      +--+-|------+
                                      |
+--------+                            |
| macrod |<---------------------------+
+--------+
```

The effect of this is that macrod's output now goes straight to libinput's
input. Any event sequence from macrod is parsed by libinput as input events
from that device and passed on as regular libinput events. Mutter already has
all the code handling those, so no extra work is required.

The only thing mutter needs to do is to trigger the events. When the socket is
given to macrod, mutter reads the `Mapped` signal for the list of
key codes macrod will parse. Any such key event should trigger macrod's
`KeyPress`/`KeyRelease` API and be discarded. In pseudo-code, this looks like this:

```
function handle_libinput_key_event(event) {
    # ignore any events not from our macrod-handled device
    if event.device is my_macrod_device:
       return process_event_normally(event)

    # if macrod doesn't handle that keycode, process normally
    if event.keycode not in self.macrod.mapped_keycodes:
       return process_event_normally(event)

    dbus_func = event.is_press ? 'KeyPress' : 'KeyRelease'

    if self.macrod.dbus_function_call(dbus_func, event.keycode):
        return  # discard event, macrod handled it

    # macrod didn't handle the key code, so process it normally
    return process_event_normally(event)
}
```

The whole stack looks like this:

```
+-------------------+
| /dev/input/event0 |
+-------------------+
              |
+-------------|-------------+              +---------------+
| libinput    |             |              | mutter        |
|   +---------v-------------+              |               |
|   |libinput_device(event0)|-[key event]->|--+ is_mapped? |
|   +-----------------------+              |  |            |                        +------+
|   |  libinput_user_dev(fd)|-[key event]-->--|------------>-[wayland key event] -> |client|
+------^--------------------+              +--|------------|                        +------+
       |                                      |
       | [macro event sequence]               |
       |                                      |
+--------+                                    |
| macrod |<-----------[KeyPress]--------------+
+--------+
```

Note: ASCII-Art has limits.

Configuration
-------------

Configuration of keys is done through .ini style configuration files. These
may be system-wide in `/etc/macrod`/ or per-user in
`$XDG_CONFIG_HOME/macrod`.

Each configuration file identifies a number of macros that apply to a
specific profile and the key sequence to emulate on a key press and/or
release.

Macro sequences are **always** in the US keyboard layout. They resemble
physical keys pressed, so uppercase letters require the correct combination
of modifiers.

Notes
-----
Where multiple macros must be function simultaneously, the caller should
create multiple devices. For example, where a compositor needs
compositor-specific macros that must always work *and* application-specific
macros, the compositor should create one macrod device with the "compositor"
profile and a second device for the application-specific profile. The
compositor is responsible for invoking macrod with the correct keys.

Limitations
-----------

### Layouts

macrod emulates a physical keyboard and is not aware of any user-configured
layouts. The macros are always in evdev scancode format. Those scancodes are
closer to "third key from top-left" but have semantic names that effectively
mean US layout. The top-left key will always send `KEY_Q` (0x18), even in a
Frency azerty layout.

Layouts are difficult and XKB-to-scancode conversion is nontrival (and
lossy). So macrod will not gain the capability for layout-specific
configuration, it's too much effort and introduces too many corner-cases.

This can be addressed by a wrapper UI that converts user input to evdev
codes, a sufficient solution for the vast majority of cases.

### Uppercase/special characters

macrod cannot send "A" or "a". It can send "shift down, A down, A up, shift
up". In other words, any macro event must resemble the physical key sequence
the user would type. See above for an explanation.

### Macros triggered by modifier shortcuts

macrod cannot trigger on shortcuts with modifier combinations (e.g.
shift+A). When macrod sends events, the modifier would still be down on the
real keyboard, so any event would be processed with shift logically down.
Fixing this is prohibitively complex and subject to race conditions and
corner case nightmares.

macrod *can* trigger on modifier-only sequences though. If you want macrod
to trigger whenever you type shift+alt but no other key, and you want that
trigger on release of both keys, that can work. The code for it doesn't
exist but it is possible.

### Multi-key triggers

multi-key triggers (e.g. 'ABC') cannot work because there's no way of
detecting whether the first few keys need to be discarded in the caller or
not. This would require extensions to the API to tell the caller to delay
the trigger key events, tell the caller to replay key events, discard them,
etc.

The complexity required in the callers to handle such cases invalidates the
raison d'etre of macrod - a super-simple integration of a generic keyboard
daemon.

### Discarding of trigger events

macrod cannot discard the trigger events, it relies on the caller to do
so. The caller must thus sit between the device and the applications
handling those key events. This is the case for Wayland compositors, or the
X server. This is the case where applications want custom macros and talk to
macrod directly or rely on toolkits to do so.

This is *not* the case where a third party listens to the keyboard events,
e.g. a process opening a device directly and using uinput to trigger macros.
For that use-case, steps need to be taken that the trigger keys don't get
processed (e.g. remove the XKB mappings for those keys).

Note: it is doubtful the X server will ever get macrod integration.

FAQ
---

### Why an external daemon?

This could all be implemented as library. However, complex macros require
delays. To avoid hanging the main thread, that library and the callers need
to add threading or forks, together with all the complications those imply.
Pushing this to an external daemon removes those complications and makes the
system easier to integrate.

### Why a daemon instead of implementing it in $COMPOSITOR

Macro support is niche and complex. It's of use primarily to power users and
while there are plenty of those, it is a tiny fraction of the users the
average compositor has. There is little chance of a macro system to get
high-enough priority to be implemente in any specific compositor, let alone
that it gets implementd in all the popular ones.

A DBus daemon has architectural drawbacks but is easiest to integrate, can
provide identical functionality regardless of the compositor and is easier
to independently extend.

### Why an evdev socket?

Ease of integration, see above. Using something else would push much of the
burden back to the compositor where there is a bottleneck of developers
already.

### What about synchronization of events

There cannot be any synchronization between true events and generated macro
events. If you have a macro that sends a key event every 200ms, then you
cannot type in the meantime until that macro is done. Events cannot be
synchronized in this case. Macros may do *anything*, it't not worth the
effort of guessing.
