#!/usr/bin/env python3
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#

import macrod

import argparse
import logging
import sys

try:
    from gi.repository import GObject, GLib
except Exception as e:
    print(f'************ Importing gi.repository failed ************')
    print(f'* This is an issue with the gi module, not with macrod *')
    print(f'********************************************************')
    print(f'The full exception is below:')
    print(f'')
    raise e


from macrod.dbusserver import MacrodDBusServer
from macrod.config import MacrodConfigParser


logging.basicConfig(format='%(levelname)s: %(name)s: %(message)s',
                    level=logging.INFO)
logger = logging.getLogger('macrod')


class Macrod(GObject.Object):
    def __init__(self):
        super().__init__()
        self.returncode = 0

        self._parse_config()

        self.server = MacrodDBusServer(self)
        self.server.connect('bus-name-lost', self._on_dbus_name_lost)
        self.mainloop = GLib.MainLoop()

    def _on_dbus_name_lost(self, dbus_server):
        self.returncode = 1
        self.mainloop.quit()

    def _parse_config(self):
        parser = MacrodConfigParser()
        self._macros = parser.parse()

    def run(self):
        self.mainloop.run()

    def new_keyboard(self, client_name):
        macros = [m for m in self._macros if m.clientmatch == client_name]
        return macrod.Keyboard(macros)


def main(args=sys.argv):
    if sys.version_info < (3, 6):
        sys.exit('Python 3.6 or later required')

    desc = 'Daemon to handle generic keyboard macros'
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-v', '--verbose',
                        help='Show some debugging informations',
                        action='store_true',
                        default=False)

    ns = parser.parse_args(args[1:])
    if ns.verbose:
        logger.setLevel(logging.DEBUG)

    macrod = Macrod()
    try:
        macrod.run()
    except KeyboardInterrupt:
        pass

    sys.exit(macrod.returncode)
