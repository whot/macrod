#!/usr/bin/env python3
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#

import enum
import struct
import time
import logging
import threading
import os

from gi.repository import GObject

import libevdev

logger = logging.getLogger('macrod')


def us2s(us):
    '''time in µs to seconds'''
    return int(us / 1000000)


def us2us(us):
    '''time in µs to µs-component'''
    return us % 1000000


def ms2us(ms):
    '''ms to µs'''
    return ms * 1000


class KeySequence(object):
    '''
    A key sequence. Use the sequence property, which is a
    list of tuples of (type, value).
    '''
    class Type(enum.Enum):
        PRESS = 1
        RELEASE = 2
        TIMEOUT = 3

    def __init__(self):
        self.sequence = []
        # Sequences for the same key cannot overlap
        self._lock = threading.Lock()

    def trigger(self, fd):
        threading.Thread(target=self._run, args=(fd,)).start()

    def _run(self, *args, **kwargs):
        fd = args[0]
        t = time.clock_gettime(time.CLOCK_MONOTONIC)
        us = int(t * 1000000)

        self._lock.acquire(True)

        for e in self.sequence:
            type, value = e
            if type == KeySequence.Type.TIMEOUT:
                us += value * 1000
                time.sleep(value / 1000.0)
                continue

            if type == KeySequence.Type.PRESS:
                v = 1
            elif type == KeySequence.Type.RELEASE:
                v = 0

            t, c = value.type.value, value.value
            bs = struct.pack('LLHHi', us2s(us), us2us(us), t, c, v)
            os.write(fd, bs)
            # SYN_REPORT
            bs = struct.pack('LLHHi', us2s(us), us2us(us), 0, 0, 0)
            os.write(fd, bs)

            logger.debug(f'Event: {us2s(us)}.{us2us(us)} {t:x} {c:x} {v}')

            # 12ms is a good interval for events
            us += ms2us(12)
        self._lock.release()


class Macro(object):
    '''
    Represents one macro
    '''
    def __init__(self):
        self.name = None  # the section name, used for debugging
        self.profile = 'none'
        self.clientmatch = None
        self.trigger = None
        self.press = KeySequence()
        self.release = KeySequence()


class Device(GObject.Object):
    __gsignals__ = {
        'mapped': (GObject.SignalFlags.RUN_FIRST, None, (GObject.TYPE_PYOBJECT,)),
    }

    def __init__(self, macros):
        super().__init__()
        self._evbits = None
        self._properties = []
        self._macros = macros
        self._profile = 'none'
        self._fd = None

    @property
    def fd(self):
        return self._fd

    @fd.setter
    def fd(self, fd):
        if self._fd is not None:
            raise ValueError('fd has already been set')
        self._fd = fd

    @property
    def evbits(self):
        '''The evdev bits required for this device to function'''
        assert self._evbits is not None  # override in subclasses

        return self._evbits

    @property
    def input_props(self):
        '''The evdev input properties required for this device to function'''
        return self._properties

    @property
    def profiles(self):
        # Profile 'none' must always exist, even if nothing was defined
        profiles = [m.profile for m in self._macros] + ['none']
        return list(set(profiles))

    @property
    def profile(self):
        return self._profile

    @profile.setter
    def profile(self, profile):
        if profile not in self.profiles:
            raise ValueError

        self._profile = profile
        mapped_keys = [m.trigger for m in self._macros if m.profile == profile]
        self.emit('mapped', mapped_keys)


class Keyboard(Device):
    def __init__(self, macros):
        super().__init__(macros)
        self._evbits = [c for c in libevdev.EV_KEY.codes if c.name.startswith('KEY_') and c.is_defined]

    def key_press(self, bit):
        for m in self._macros:
            if m.trigger == bit and m.press is not None:
                m.press.trigger(self._fd)
                return True

        return False

    def key_release(self, bit):
        for m in self._macros:
            if m.trigger == bit and m.release is not None:
                m.release.trigger(self._fd)
                return True

        return False
