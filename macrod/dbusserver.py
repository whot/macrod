#!/usr/bin/env python3
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#

import logging

from gi.repository import GObject, Gio, GLib

import libevdev

logger = logging.getLogger('macrod.dbus')

BUS_NAME = 'org.freedesktop.macrod1'
BASE_PATH = '/org/freedesktop/macrod1'

INTROSPECTION_XML = '''
<node>
  <interface name='org.freedesktop.macrod1.Macrod'>
    <method name='GetKeyboard'>
      <arg name='clientname' type='s' direction='in'/>
      <arg name='keyboard' type='o' direction='out'/>
    </method>
  </interface>

  <interface name='org.freedesktop.macrod1.Device'>
    <property type='a(qq))' name='EvdevBits' access='read'/>
    <property type='aq' name='InputProperties' access='read'/>
    <property type='as' name='Profiles' access='read'>
      <annotation name='org.freedesktop.DBus.Property.EmitsChangedSignal' value='true'/>
    </property>

    <method name='Destroy'>
      <annotation name='org.freedesktop.DBus.Method.NoReply' value='true'/>
    </method>

    <method name='SetEvdevFd'>
      <arg name='fd' type='h' direction='in'/>
      <annotation name='org.freedesktop.DBus.Method.NoReply' value='true'/>
    </method>

    <method name='KeyPress'>
      <arg name='key' type='u' direction='in'/>
      <arg name='result' type='b' direction='out'/>
    </method>

    <method name='KeyRelease'>
      <arg name='key' type='u' direction='in'/>
      <arg name='result' type='b' direction='out'/>
    </method>

    <method name='SetProfile'>
      <arg name='profile' type='s' direction='in'/>
      <annotation name='org.freedesktop.DBus.Method.NoReply' value='true'/>
    </method>

    <signal name='Mapped'>
      <arg name='bits' type='a(q, q))'/>
    </signal>

    <signal name='Error'>
      <arg name='message' type='s'/>
    </signal>
  </interface>
</node>
'''


class _DbusObjWrapper(GObject.Object):
    '''
    A wrapper class to handle signals and property changes in a more generic
    fashion.
    '''

    INTERFACE_NAME = None

    def __init__(self, connection, objpath):
        assert self.INTERFACE_NAME is not None

        super().__init__()
        self.connection = connection
        self.objpath = objpath
        self._dbus_id = None

        self.dbus_methods = {}
        self.dbus_properties = {}

    def dbus_register(self):
        assert self.connection is not None

        introspection = Gio.DBusNodeInfo.new_for_xml(INTROSPECTION_XML)
        intf = introspection.lookup_interface(self.INTERFACE_NAME)
        self._dbus_id = self.connection.register_object(self.objpath,
                                                        intf,
                                                        self._method_cb,
                                                        self._property_read_cb,
                                                        self._property_write_cb)

    def dbus_unregister(self):
        if self._dbus_id is not None:
            self.connection.unregister_object(self._dbus_id)
            self._dbus_id = None

    def _method_cb(self, connection, sender, objpath, interface, methodname, args, invocation):
        if interface != self.INTERFACE_NAME:
            return None

        try:
            method = self.dbus_methods[methodname]
        except KeyError:
            logger.info(f'Unknown method "{methodname}"')
            return None

        logger.debug(f'Invoking {methodname}() for {sender}, args {args}')

        # convert the input arguments from the GVariant with separate fd
        # list to a single list of values, where the fds are actual fds.
        message = invocation.get_message()
        fd_list = message.get_unix_fd_list()
        if fd_list:
            fd_list = fd_list.steal_fds()

        args_converted = []
        idx = 0
        for in_arg in invocation.get_method_info().in_args:
            if in_arg.signature != 'h':
                args_converted.append(args[idx])
            else:
                if not fd_list:
                    logger.error(f'{methodname} requires fd for arg "{in_arg.name}" but none given')
                    return None
                args_converted.append(fd_list.pop(0))
            idx += 1

        args = args_converted

        r = method(connection, sender, objpath, args)
        if r is not None:
            t = GLib.Variant.new_tuple(r)
            # FIXME: make this more generic
            if r.get_type_string() == 'h':
                fd_list = Gio.UnixFDList.new()
                fd_list.append(r.get_handle())
                invocation.return_value_with_unix_fd_list(t, fd_list)
            else:
                invocation.return_value(t)
        else:
            invocation.return_value()

    def _property_read_cb(self, connection, sender, objpath, interface, propname):
        if interface != self.INTERFACE_NAME:
            return None

        try:
            prop = self.dbus_properties[propname]
            return prop(connection, sender, objpath, propname)
        except KeyError:
            logger.info(f'Unknown property "{propname}"')

    def _property_write_cb(self, connection, sender, objpath, interface, propname):
        if interface != self.INTERFACE_NAME:
            return None

        # FIXME: return a DBus error here
        pass

    def properties_changed(self, props, dest=None):
        '''
        Send a PropertiesChanged signal to the given destination (if any).
        The props argument is a { name: value } dictionary of the
        property values, the values are GVariant.bool, etc.
        '''
        builder = GLib.VariantBuilder(GLib.VariantType('a{sv}'))
        for name, value in props.items():
            de = GLib.Variant.new_dict_entry(GLib.Variant.new_string(name),
                                             GLib.Variant.new_variant(value))
            builder.add_value(de)
        properties = builder.end()
        iname = GLib.Variant.new_string(self.INTERFACE_NAME)
        inval_props = GLib.VariantBuilder(GLib.VariantType('as'))
        inval_props = inval_props.end()
        args = GLib.Variant.new_tuple(iname, properties, inval_props)
        self.connection.emit_signal(dest, self.objpath,
                                    'org.freedesktop.DBus.Properties',
                                    'PropertiesChanged',
                                    args)

    def signal(self, name, arg=None, dest=None):
        if arg is not None:
            arg = GLib.Variant.new_tuple(arg)
        self.connection.emit_signal(dest, self.objpath, self.INTERFACE_NAME, name, arg)


class MacrodDBusDevice(_DbusObjWrapper):
    device_counter = 0

    INTERFACE_NAME = 'org.freedesktop.macrod1.Device'

    def __init__(self, connection, sender, client_name, device):
        objpath = f'{BASE_PATH}/dev_{client_name}_{MacrodDBusDevice.device_counter}'
        MacrodDBusDevice.device_counter += 1
        super().__init__(connection, objpath)

        self._macrod_device = device
        self._macrod_device.connect('mapped', self._on_mapped_signal)
        self._sender = sender
        self._client_name = client_name
        self.dbus_methods = {
            'Destroy': self._cb_destroy,
            'SetEvdevFd': self._cb_evdev_fd,
            'KeyPress': self._cb_key_press,
            'KeyRelease': self._cb_key_release,
            'SetProfile': self._cb_set_profile,
        }
        self.dbus_properties = {
            'EvdevBits': self._cb_prop_evdev_bits,
            'InputProperties': self._cb_prop_input_props,
            'Profiles': self._cb_prop_profiles,
        }

        self.dbus_register()
        logger.debug(f'{self.objpath}: created for {client_name}, source {sender}')

    def remove(self):
        self.dbus_unregister()

    def _on_mapped_signal(self, macrod_device, mapped):
        bits = self._evdev_bits_to_variant_array(mapped)
        self.signal('Mapped', bits, dest=self._sender)

    def _cb_destroy(self, connection, sender, objpath, args):
        if sender != self._sender:
            return None

        self.remove()

    def _cb_evdev_fd(self, connection, sender, objpath, args):
        if sender != self._sender:
            return None

        self._macrod_device.fd = args[0]

    def _cb_key_press(self, connection, sender, objpath, args):
        if sender != self._sender:
            return None

        key = libevdev.evbit('EV_KEY', args[0])
        return GLib.Variant.new_boolean(self._macrod_device.key_press(key))

    def _cb_key_release(self, connection, sender, objpath, args):
        if sender != self._sender:
            return None

        key = libevdev.evbit('EV_KEY', args[0])
        return GLib.Variant.new_boolean(self._macrod_device.key_release(key))

    def _cb_set_profile(self, connection, sender, objpath, args):
        if sender != self._sender:
            return None

        profile = args[0]
        self._macrod_device.profile = profile
        return None

    def _evdev_bits_to_variant_array(self, bits):
        builder = GLib.VariantBuilder(GLib.VariantType('a(qq)'))
        for bit in bits:
            builder.open(GLib.VariantType('(qq)'))
            builder.add_value(GLib.Variant.new_uint16(bit.type.value))
            builder.add_value(GLib.Variant.new_uint16(bit.value))
            builder.close()
        return builder.end()

    def _cb_prop_evdev_bits(self, connection, sender, objpath, propname):
        if sender != self._sender:
            return None

        return self._evdev_bits_to_variant_array(self._macrod_device.evbits)

    def _cb_prop_input_props(self, connection, sender, objpath, propname):
        if sender != self._sender:
            return None

        return GLib.Variant.new_array(GLib.VariantType('q'),
                                      [GLib.Variant.new_uint16(p) for p in
                                          self._macrod_device.input_props])

    def _cb_prop_profiles(self, connection, sender, objpath, propname):
        if sender != self._sender:
            return None

        return GLib.Variant.new_array(GLib.VariantType('s'),
                                      [GLib.Variant.new_string(p) for p in
                                          self._macrod_device.profiles])


class MacrodDBusServer(_DbusObjWrapper):
    INTERFACE_NAME = 'org.freedesktop.macrod1.Macrod'

    __gsignals__ = {
        'bus-name-acquired':
            (GObject.SignalFlags.RUN_FIRST, None, ()),
        'bus-name-lost':
            (GObject.SignalFlags.RUN_FIRST, None, ()),
    }

    def __init__(self, macrod):
        super().__init__(None, BASE_PATH)
        self._macrod = macrod
        self._devices = []
        self._dbus = Gio.bus_own_name(Gio.BusType.SESSION,
                                      BUS_NAME,
                                      Gio.BusNameOwnerFlags.NONE,
                                      self._bus_aquired,
                                      self._bus_name_aquired,
                                      self._bus_name_lost)
        self.dbus_methods = {'GetKeyboard': self._cb_get_keyboard}

    def _bus_aquired(self, connection, name):
        pass

    def _bus_name_aquired(self, connection, name):
        logger.debug('Bus name aquired')
        self.emit('bus-name-acquired')
        self.connection = connection
        self.dbus_register()

    def _bus_name_lost(self, connection, name):
        logger.error('Bus not available, is there another macrod process running?')
        self.emit('bus-name-lost')
        self.connection = None

    def cleanup(self):
        self.dbus_unregister()
        Gio.bus_unown_name(self._dbus)

    def _cb_get_keyboard(self, connection, sender, objpath, args):
        name = args[0]
        kbd = self._macrod.new_keyboard(name)
        keyboard = MacrodDBusDevice(connection, sender, name, kbd)
        return GLib.Variant.new_object_path(keyboard.objpath)
