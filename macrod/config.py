#!/usr/bin/env python3
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#

from macrod import KeySequence, Macro

import logging
from configparser import ConfigParser
import os
from pathlib import PurePath, Path

import libevdev

logger = logging.getLogger('macrod.config')


def search_directories():
    paths = []
    paths.append(PurePath('.'))
    paths.append(PurePath('/etc/macrod/'))

    try:
        home = os.environ['XDG_CONFIG_HOME']
    except KeyError:
        home = Path.home()
    finally:
        p = PurePath(home, 'macrod')
        paths.append(p)
    return paths


class ParserError(Exception):
    pass


class MacrodConfigParser(object):
    def __init__(self):
        self.paths = search_directories()

    def parse(self):
        macros = []

        files = []
        for path in self.paths:
            p = Path(path)
            for ini in p.glob('*.ini'):
                logger.debug(f'config file: {ini}')
                files.append(ini)

        # We sort across all directories. Where files are identically named,
        # the behaviour is... undefined? So don't do that then.
        files = sorted(files, key=lambda p: PurePath(p.resolve()).name)

        for f in files:
            c = ConfigParser()
            c.read_file(f.open())

            try:
                profile_section = c['Profile']
            except KeyError:
                logger.error(f'Invalid or missing [Profile] section in {f.resolve()}. Skipping file')
                continue

            profile = profile_section.get('Name', 'none')
            client = profile_section.get('ClientMatch', None)

            for s in c.sections():
                if s == 'Profile':
                    continue

                try:
                    trigger = c[s]['KeyTrigger']
                except KeyError:
                    logger.error(f'Missing KeyTrigger in [{s}] in {f.resolve()}. Skipping section')
                    continue

                press = c[s].get('KeySequencePress')
                release = c[s].get('KeySequenceRelease')
                if press is None and release is None:
                    logger.error(f'Require one of KeySequencePress/Release in [{s}] in {f.resolve()}. Skipping section')
                    continue

                m = Macro()
                m.name = s
                m.profile = profile
                m.clientmatch = client
                try:
                    m.trigger = self.parse_trigger(trigger)
                except ParserError:
                    logger.error(f'Invalid trigger {trigger} in [{s}] in {f.resolve()}. Skipping section')
                    continue

                try:
                    m.press = self.parse_sequence(press)
                except ParserError:
                    logger.error(f'Failed to parse {press} in [{s}] in {f.resolve()}. Skipping section')
                    continue

                try:
                    m.release = self.parse_sequence(release)
                except ParserError:
                    logger.error(f'Failed to parse {release} in [{s}] in {f.resolve()}. Skipping section')
                    continue

                macros.append(m)

        return macros

    def parse_trigger(self, trigger):
        if not trigger.startswith('KEY_'):
            trigger = f'KEY_{trigger}'

        t = libevdev.evbit(trigger)
        if t is None:
            raise ParserError()

        return t

    def parse_sequence(self, string):
        if string is None:
            return None

        s = KeySequence()

        if string[-1] == ';':
            string = string[:-1]
        elements = string.split(';')
        for e in elements:
            if not e:
                raise ParserError()

            if e[0] == 't':
                try:
                    timeout = int(e[1:])
                    s.sequence.append((KeySequence.Type.TIMEOUT, timeout))
                except ValueError:
                    raise ParserError()
                continue

            is_press = False
            is_release = False

            if e.startswith('+'):
                is_press = True
                e = e[1:]
            elif e.startswith('-'):
                is_release = True
                e = e[1:]
            else:
                is_press = True
                is_release = True

            if not e.startswith('KEY_'):
                e = f'KEY_{e}'

            bit = libevdev.evbit(e)
            if bit is None:
                raise ParserError()

            if is_press:
                s.sequence.append((KeySequence.Type.PRESS,
                                  bit))
            if is_release:
                s.sequence.append((KeySequence.Type.RELEASE,
                                  bit))
        if not s.sequence:
            raise ParserError()

        return s
