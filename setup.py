#!/usr/bin/env python3

from setuptools import setup

setup(name='macrod',
      version='0',
      description='A daemon to handle generic keyboard event macros',
      long_description=open('README.md', 'r').read(),
      url='http://gitlab.freedesktop.org/libevdev/macrod',
      packages=['macrod'],
      author='The macrod Developers',
      author_email='check-gitlab-for-contributors@example.com',
      license='GPL',
      entry_points={
          'console_scripts': ['macrod = macrod.base:main'],
      },
      classifiers=[
          'Development Status :: 3 - Alpha',
          'License :: OSI Approved :: GNU General Public License v2 or later (GPLv2+)',
          'Programming Language :: Python :: 3',
          'Programming Language :: Python :: 3.6'
      ],
      python_requires='>=3.6',
      install_requires=[
          'libevdev',
          'pygobject',
      ]
      )
