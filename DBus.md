DBus API
========

```
org.freedesktop.macrod1.Macrod

    Method: GetKeyboard(s) -> (o)
        Creates a keyboard device for the calling client and returns the
        object path. This object will only respond to the client that
        created it in the first place.

        The string argument is the name of the caller, e.g. "mutter". This
        name has no meaning to macrod but it restricts which macros that
        caller will see. See the configuration file format for more details.

org.freedesktop.macrod1.Device

    Property: EvdevBits (a(q,q))
        An array of evdev types supported. The first element of each tuple
        is the evdev type, the second element the evdev codes. For
        example:
        [(EV_KEY, KEY_A), (EV_KEY, KEY_B), (EV_REL, REL_X)]
        The caller must use this array to create the evdev device for
        macrod.

        This property is immutable.

    Property: InputProperties (a(q))
        An array of evdev input properties. The caller must use this array
        to create the evdev device for macrod.

        This property is immutable.

    Property: Profiles (a(s))
        An array of strings representing user-specified profiles that the
        caller may select for emulation. The profile "none" is always
        available.

        This property is mutable, macrod may reload profiles from disk at
        runtime.

    Method: Destroy() -> ()
        Destroy this object. This is done automatically when the client
        disappears from the bus.

    Method: SetEvdevFd(h) -> ()
        Set the file descriptor for this device. Once the file descriptor
        has been given to the macrod, the device may be used for macro
        emulation.

        macrod treats the file descriptor like a file descriptor to a
        /dev/input/eventX node, i.e. it writes valid evdev protocol to that
        descriptor (in the form of struct input_event). It will not use
        ioctl(2) on the fd however.

        This method triggers the Mapped signal.
        This method may only be called once, a subsequent invocation will
        trigger an error.

    Method: KeyPress(u) -> (b)
        Trigger a key press event on this device. If the caller must
        discard the event, this method returns True.
        The return code reflects the keys in the Mapped signal.

    Method: KeyRelease(u) -> (b)
        Trigger a key press event on this device. If the caller must
        discard the event, this method returns True.
        The return code reflects the keys in the Mapped signal.

    Method: SetProfile(s) -> ()
        Set the given profile as the current emulation profile. This profile
        must match one of the values in the Profiles property. Where a
        given profile does not exist, the call is silently ignored.

        This method triggers the Mapped signal.

    Signal: Mapped -> (a(q, a(q)))
        A signal notifying the caller which evdev bits are mapped to macros,
        in the same format as the EvdevBits property.
        For any evdev bit set, the KeyPress and KeyRelease methods return
        True.

    Signal: Error -> (s)
        A signal notifying the caller that an internal error has occured and
        that the device is no longer reliable. A caller must call
        Destroy() on the device and discard any internal references to it.

        The string provided is a human-readable error message that may be
        printed to logs. It is for developer consumption, not necessarily
        end-user consumption.
```
